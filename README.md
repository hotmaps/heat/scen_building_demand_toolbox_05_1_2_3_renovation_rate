[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687105.svg)](https://doi.org/10.5281/zenodo.4687105)

# Energy demand scenarios in buildings until the year 2050 - scenarios with refurbishment rate (maintenance + thermal renovation) of 0.5%, 1%, 2% and 3%


## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
```

## Documentation

This datasets includes scenario results for the development of energy demand for space heating and domestic hot water on NUTS0 level for EU 28.
The results are based on the model [Invert/EE-Lab](https://www.invert.at)[1]. The modelling work was carried out within the Hotmaps project, the dataset has been implemented into the Hotmaps Toolbox and is used by the Demand Projection Calculation Module.


There, the scenarios are used as a baseline to estimate potential developments of energy demand in EU28 in order to compare results on local developments with overal efficiency and emission mitigation targets on country and EU28 level.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps: D5.2 Heating & Cooling outlook until 2050](http://www.hotmaps-project.eu/library/) 




### References
[1] [Invert EE-Lab model](https://www.invert.at)
[2] [Hotmaps: D5.2 Heating & Cooling outlook until 2050, EU-28, 2018] (http://www.hotmaps-project.eu/library/)


## How to cite


Lukas Kranzl, Michael Hartner, Andreas Müller, Gustav Resch, Sara Fritz (TUW), Andreas Müller (e‐think),Tobias Fleiter, Andrea Herbst, Matthias Rehfeldt, Pia Manzi (Fraunhofer ISI,)
Alyona Zubaryeva (EURAC), reviewed by Jakob Rager (CREM): Hotmaps Project, D5.2 Heating & Cooling outlook until 2050, EU-28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/library/) 


## Authors
Andreas Müller <sup>*</sup>,
Mostafa Fallahnejad <sup>*</sup>,



<sup>*</sup> [e‐think energy research](https://e-think.ac.at/)
Argentinierstr. 18/10
1040 Wien



## License
This work is licensed under a CC BY (Creative Commons By Attribution)

Copyright © 2016-2020: Andreas Müller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
